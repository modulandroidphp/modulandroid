package testk24willybrodusrangga.org.testk24willybrodusrangga;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class InputActiity extends AppCompatActivity {

    private EditText nama, alamat, user, password;
    private Button simpan;
    private String txtnama, txtalamat, txtuser, txtpassword;
    private String sidx;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_actiity);

        progress = new ProgressDialog(this);
        progress.setMessage("Please Waiate");
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setIndeterminate(true);

        nama = (EditText) findViewById(R.id.nama);
        alamat = (EditText) findViewById(R.id.alamat);
        user = (EditText) findViewById(R.id.user);
        password = (EditText) findViewById(R.id.password);

        simpan = (Button) findViewById(R.id.submite);

        Bundle b = getIntent().getExtras();
        if(b!=null) {
            String jsonIsiUserActivity = b.getString("jsonIsiUserActivity");
            Log.d(" jsonIsiUserActivity", jsonIsiUserActivity);
            Gson gson = new Gson();
            UserCLASS IsiUserActivity = gson.fromJson(jsonIsiUserActivity, UserCLASS.class);
            sidx= IsiUserActivity.getIdx() ;
            nama.setText(IsiUserActivity.getNama());
            alamat.setText(IsiUserActivity.getAlamat());
            user.setText(IsiUserActivity.getUser());
            password.setText(IsiUserActivity.getPassword());
        }
        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtnama = nama.getText().toString();
                txtalamat = alamat.getText().toString();
                txtuser = user.getText().toString();
                txtpassword = password.getText().toString();
                Log.d("nama", nama.getText().toString());
                new Simpanmember(txtnama, txtalamat, txtuser,txtpassword).execute();

            }
        });

    }

    public class Simpanmember extends AsyncTask<Void, Void, ArrayList<UserCLASS>> {

        private ArrayList<UserCLASS> listdataCombo;
        String inama, ialamat, iuser, ipassword;

        Simpanmember(String pnama, String palamat, String puser, String ppassword) {
            this.inama = pnama;
            this.ialamat = palamat;
            this.iuser = puser;
            this.ipassword = ppassword;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

//            progress.show();
        }

        @Override
        protected ArrayList<UserCLASS> doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            Gson gson = new Gson();

            BufferedReader bufreader = null;
            URL url = null;
            try {
                url = new URL(config.SERVER_PHP + "user/simpanupdateuserAndroid");
//                url = new URL(Config.SERVER_PHP + "ctrmember/ujicoba/");
                HttpURLConnection urlConnection = null;
                try {
                    urlConnection = (HttpURLConnection) url.openConnection();
//                    urlConnection.setReadTimeout(30000);
//                    urlConnection.setConnectTimeout(35000);
//                    urlConnection.setRequestMethod("POST");
                    urlConnection.setDoInput(true);
                    urlConnection.setDoOutput(true);


                    Uri.Builder builder = new Uri.Builder()
                            .appendQueryParameter("edidx", sidx + "")
                            .appendQueryParameter("ednama", inama + "")
                            .appendQueryParameter("edalamat", ialamat + "")
                            .appendQueryParameter("eduser", iuser + "")
                            .appendQueryParameter("edpassword", ipassword + "");

                    String query = builder.build().getEncodedQuery();

                    OutputStream os = urlConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(
                            new OutputStreamWriter(os, "UTF-8"));
                    writer.write(query);
                    writer.flush();
                    writer.close();
                    os.close();

                    //urlConnection.connect();

                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    bufreader = new BufferedReader(new InputStreamReader(
                            urlConnection.getInputStream()));
//////
//                    String  line;
//                    String Sresponse="";
//
//                    while ((line=bufreader.readLine()) != null) {
//                        Sresponse+=line;
//                    }
//                    Log.d("Data hasil Check ", "> " + Sresponse+"\n");

                    listdataCombo = gson.fromJson(bufreader, new TypeToken<List<UserCLASS>>() {
                    }.getType());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }


            return listdataCombo;
        }

        @Override
        protected void onPostExecute(final ArrayList<UserCLASS> xlistDataCombo) {
            progress.dismiss();
            if (xlistDataCombo != null) {
                Toast.makeText(getApplication(), "Sukses", Toast.LENGTH_LONG).show();
                Intent inte=new Intent(InputActiity.this, MainActivity.class);
                startActivity(inte);

            } else {
                Toast.makeText(getApplication(), "Gagal Save", Toast.LENGTH_LONG).show();
            }

        }
    }
}
