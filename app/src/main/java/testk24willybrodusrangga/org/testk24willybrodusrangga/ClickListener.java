package testk24willybrodusrangga.org.testk24willybrodusrangga;

import android.view.View;

/**
 * Created by jhonpc on 8/28/2016.
 */
public interface ClickListener {
    void onClick(View view, int position);

    void onLongClick(View view, int position);
}