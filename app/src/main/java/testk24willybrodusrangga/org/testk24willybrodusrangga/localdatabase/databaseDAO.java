package testk24willybrodusrangga.org.testk24willybrodusrangga.localdatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import testk24willybrodusrangga.org.testk24willybrodusrangga.UserCLASS;
import testk24willybrodusrangga.org.testk24willybrodusrangga.dataCLASS;

/**
 * Created by WR on 2/11/2017.
 */

public class databaseDAO {
    private final String namatable = "data";
    private SQLiteDatabase database;
    private Context context;
    private buatdatabaselocal dbHelper;

    private String[] allColumns = {
            "no",
            "nama",
            "alamat",
            "user",
            "password"};

    public databaseDAO(Context contex) {
        this.context =  contex;
        dbHelper = new buatdatabaselocal(contex);

    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public UserCLASS getdata(String xid) {
        ContentValues values = new ContentValues();
        //String xWhere =  + " = \""+ xid+"\"";
        values.put(allColumns[0], xid);
        Cursor cursor = database.query(namatable,
                allColumns, allColumns[0]+"=?" , new String[] {xid}, null, null, null);

        UserCLASS dataApiclass = null;
        if (cursor.moveToFirst()) {
            dataApiclass = getdataApi(cursor);
        }
        cursor.close();
        return dataApiclass;
    }


    public UserCLASS getdataApi(Cursor cursor) {
        UserCLASS dataApiclass = new UserCLASS();
        dataApiclass.setIdx(cursor.getString(0));
        dataApiclass.setNama(cursor.getString(1));
        dataApiclass.setAlamat(cursor.getString(2));
        dataApiclass.setUser(cursor.getString(3));
        dataApiclass.setPassword(cursor.getString(4));


        return dataApiclass;
    }


    public void insertdata(UserCLASS dataApi) {
        ContentValues values = new ContentValues();
        values.put(allColumns[0], dataApi.getIdx());
        values.put(allColumns[1], dataApi.getNama());
        values.put(allColumns[2], dataApi.getAlamat());
        values.put(allColumns[3], dataApi.getUser());
        values.put(allColumns[4], dataApi.getPassword());
        Log.d("Isi helper", dataApi.getNama());


//        Toast.makeText(context, "On Insert Data Base "+dataApi.getphonenumber(), Toast.LENGTH_LONG).show();
        database.insert(namatable, null, values);
        Log.d("Menggunakan : ", "insertdata method");

    }


    public void updatedataApi(UserCLASS dataApi) {
        ContentValues values = new ContentValues();
        values.put(allColumns[0], dataApi.getIdx());
        values.put(allColumns[1], dataApi.getNama());
        values.put(allColumns[2], dataApi.getAlamat());
        values.put(allColumns[3], dataApi.getUser());
        values.put(allColumns[4], dataApi.getPassword());

        String whereClause = allColumns[0] + "=" + dataApi.getIdx();
        database.update(namatable, values, whereClause, null);
    }


    public List<UserCLASS> getListdataApi() {

        Log.d("Menggunakan : ", "getListdata method");
        List<UserCLASS> listdataApi = new ArrayList<UserCLASS>();
        Cursor cursor = database.query(namatable,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UserCLASS dataApi = getdataApi(cursor);
            listdataApi.add(dataApi);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return listdataApi;
    }


    public List<UserCLASS> getListdataApi(String WhereLike) {
        List<UserCLASS> listdataApi = new ArrayList<UserCLASS>();
        Cursor cursor = database.query(namatable,
                allColumns, WhereLike, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            UserCLASS dataApi = getdataApi(cursor);
            listdataApi.add(dataApi);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return listdataApi;
    }


    public void setDeletedataApi() {
        database.delete(namatable, null, null);
    }

    public void setDeletedataApi(String xWhere) {
        database.delete(namatable, xWhere, null);
    }

//    public void isEksis() {
//        database.;
//    }
}
